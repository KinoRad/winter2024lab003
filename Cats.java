public class Cats{
	public int cutenessOfCat;
	public String textureOfCat;
	public String nameOfCat;
	
	public Cats(int cutenessOfCat, String textureOfCat, String nameOfCat){
		this.cutenessOfCat = cutenessOfCat;
		this.textureOfCat = textureOfCat;
		this.nameOfCat = nameOfCat;
	}
	public String talks(){
		return nameOfCat + " says meaow! or something...";
	}
	public String isCute(){
		if(cutenessOfCat > 9){
			return "it is reaaaaaaaaaaaaaaaallly cute!";
		} else if(cutenessOfCat > 7){
			return "it is cute!";
		} else{
			return "ewwwwwwww!";
		}
	}
}